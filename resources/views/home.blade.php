@extends('layouts.app')

@section('content')
<div class="container mt40">
    <div class="row">
        @foreach($images as $image)
            <article class="col-xs-12 col-sm-6 col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="{{URL::asset($image->file_path)}}" title="{{ $image->data()->rand }}" class="zoom" data-title="{{ $image->id }}" data-footer="{{ $image->data()->rand }}" data-type="image" data-toggle="lightbox">
                            <img src="{{URL::asset($image->file_path)}}" alt="{{ $image->data()->rand }}" />
                            <span class="overlay"><i class="glyphicon glyphicon-fullscreen"></i></span>
                        </a>
                    </div>
                    <div class="panel-footer">
                        <h4><a href="{{URL::asset($image->file_path)}}" title="{{ $image->data()->rand }}">{{ $image->data()->rand }}</a></h4>
                        <span class="pull-right">
                            <i id="like{{ $image->id }}" class="glyphicon glyphicon-thumbs-up"></i> <div id="like{{ $image->id }}-bs3"></div>
                            <i id="dislike{{ $image->id }}" class="glyphicon glyphicon-thumbs-down"></i> <div id="dislike{{ $image->id }}-bs3"></div>
                        </span>
                    </div>
                </div>
            </article>
        @endforeach
    </div>
</div>

@endsection
