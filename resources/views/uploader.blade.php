@extends('layouts.app')

@section('content')

<div class="container">
      <div class="panel panel-default" id="uploader">
        <div class="panel-heading"><strong>Upload Images</strong> <small>Simple Uploader</small></div>
        <div class="panel-body">

          <!-- Standar Form -->
          <h4>Select files from your computer</h4>
          <form action="" method="post" enctype="multipart/form-data" id="js-upload-form">
            {{ csrf_field() }}
            @if (!Auth::guest())
            <input type="hidden" name="user_id" value="{!! Auth::id() !!}" />
            @endif
            <div class="form-inline">
              <div class="form-group">
                <input type="file" name="files[]" id="js-upload-files" multiple>
              </div>
              <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit">Upload files</button>
            </div>
          </form>

          <!-- Drop Zone -->
          <h4>Or drag and drop files below</h4>
          <div class="upload-drop-zone" id="drop-zone">
            Just drag and drop files here
          </div>

          <!-- Progress Bar -->
          <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
              <span class="sr-only">0% Complete</span>
            </div>
          </div>

          <!-- Upload Finished -->
          <div class="js-upload-finished">
            <h3>Processed files</h3>
            <div class="list-group">
              
            </div>
          </div>
        </div>
      </div>
    </div> <!-- /container -->

@endsection
