
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');



jQuery(document).ready(function($) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': window.Laravel.csrfToken
        }
    });

    $('#myCarousel').carousel({
            interval: 5000
    });

    //Handles the carousel thumbnails
    $('[id^=carousel-selector-]').click(function () {
	    var id_selector = $(this).attr("id");
	    try {
	        var id = /-(\d+)$/.exec(id_selector)[1];
	        console.log(id_selector, id);
	        jQuery('#myCarousel').carousel(parseInt(id));
	    } catch (e) {
	        console.log('Regex failed!', e);
	    }
	});
    // When the carousel slides, auto update the text
    $('#myCarousel').on('slid.bs.carousel', function (e) {
             var id = $('.item.active').data('slide-number');
            $('#carousel-text').html($('#slide-content-'+id).html());
    });


    $("#uploader .progress").hide();
    $("#uploader .js-upload-finished").hide();


    var dropZone = $('#drop-zone');
    var uploadForm = $('#js-upload-form');

    var startUpload = function(files) {
        var formData = new FormData();
        for(var i=0; i<files.length; i++){
            formData.append('files['+i+']', files[i]);
        }
        var other_data = $('#js-upload-form').serializeArray();
        $.each(other_data,function(key,input){
            formData.append(input.name,input.value);
        });

        $.ajax({
            url: "/api/storage",
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            beforeSend: function(xhr)
              {
                $("#uploader .progress").show();
                //Upload progress
                $(xhr).on("progress", function(evt){
                  if (evt.lengthComputable) {  
                    var percentComplete = evt.loaded / evt.total;
                    $("#uploader .progress .progress-bar").attr('aria-valuenow', percentComplete);
                    $("#uploader .progress .progress-bar").css('style', percentComplete + '%');
                    $("#uploader .progress .progress-bar .sr-only").text(percentComplete + '%');
                  }
                }, false); 
              },
            success: function(data){
                $("#uploader .progress").hide();
                $("#uploader .js-upload-finished").show();

                var template = '<a href="#" class="list-group-item list-group-item-success"><span class="badge alert-success pull-right">Success</span><span class="filename">image-01</span></a>';
                
                
                $.each(data,function(i,e){
                    var temp = $(template);
                    temp.find('.filename').text(e.file);
                    $('.js-upload-finished').find('.list-group').append(temp);
                });
            },
            error: function(data){
                $("#uploader .progress").hide();
                $("#uploader .js-upload-finished").show();

                var template = '<a href="#" class="list-group-item list-group-item-error"><span class="badge alert-error pull-right">Error</span><span class="filename">image-01</span></a>';
                
                
                $.each(data,function(i,e){
                    var temp = $(template);
                    temp.find('.filename').text(e.file);
                    $('.js-upload-finished').find('.list-group').append(temp);
                });
            }
        });
    }

    uploadForm.on('submit', function(e) {
        var uploadFiles = document.getElementById('js-upload-files').files;
        e.preventDefault()

        startUpload(uploadFiles)
    });

    dropZone.on('dragover', function(){
        this.className = 'upload-drop-zone drop';
        return false;
    }).on('drop', function(e){
        e.preventDefault();
        e.stopPropagation();
        this.className = 'upload-drop-zone';

        startUpload(e.originalEvent.dataTransfer.files)
    }).on('dragleave', function(){
        this.className = 'upload-drop-zone';
        return false;
    });

     $('i.glyphicon-thumbs-up, i.glyphicon-thumbs-down').click(function(){    
        var $this = $(this),
        c = $this.data('count');    
        if (!c) c = 0;
        c++;
        $this.data('count',c);
        $('#'+this.id+'-bs3').html(c);
    });      
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });  

    
});