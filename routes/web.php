<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Guest@index');
Route::get('/upload', 'Guest@upload')->name('upload');

Auth::routes();

Route::group(['prefix' => 'profile'], function(){
	Route::match(['GET', 'POST'], 'myprofile', 'ProfileController@myProfile');
	Route::match(['GET', 'POST'], 'myuploads', 'ProfileController@myUploads');
});

Route::any('captcha-test', function()
{
    if (Request::getMethod() == 'POST')
    {
        $rules = ['captcha' => 'required|captcha'];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            echo '<p style="color: #ff0000;">Incorrect!</p>';
        }
        else
        {
            echo '<p style="color: #00ff30;">Matched :)</p>';
        }
    }

    $form = '<form method="post" action="captcha-test">';
    $form .= '<input type="hidden" name="_token" value="' . csrf_token() . '">';
    $form .= '<p>' . captcha_img() . '</p>';
    $form .= '<p><input type="text" name="captcha" class></p>';
    $form .= '<p><button type="submit" name="check">Check</button></p>';
    $form .= '</form>';
    return $form;
});



Route::get('/images/{filename}', function($filename){
    $file = Storage::get('images/'.$filename);
    $mimetype = Storage::mimeType('images/'.$filename);

     if (!$file) {
        abort(404);
    }

    $response = Response::make($file, 200);
    $response->header("Content-Type", $mimetype);

    return $response;
});