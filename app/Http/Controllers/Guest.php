<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image as Image;
use App\Category as Category;

class Guest extends Controller
{
    public function index(){
        $images = Image::all();
        $categories = Category::all();
        return view('home')->with(['images' => $images, 'categories' => $categories]);
    }

    public function upload(){
    	return view('uploader');
    }
}
