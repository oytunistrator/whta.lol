<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Str;
use App\Image as Image; 
use App\User as User;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $saved = [];
        $files = $request->file('files');
        $input = $request->all();
        foreach ($files as $key => $file) {
            $filename = $file->store('images');
            if($filename){ 
                $image = new Image; 
                if ($input['user_id']) { 
                    $user = User::find($input['user_id']);
                    $userData['request'] = $request->all(); 
                    $userData['userdata'] = $user; 
                    $image->user_id = $user->id; 
                } 
                $userData['ip'] = $request->server('REMOTE_ADDR'); 
                $userData['cookies'] = $request->cookies->all(); 
                $userData['rand'] = Str::quickRandom(20);
                $image->data = json_encode($userData); 
                $image->file_path = $filename; 
                $image->save();
                $saved[]['file'] = $image->file_path; 
            }
        }
        return response()->json($saved); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Image::find($id);
        return response()->json($store); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Image::delete($id);
        return response()->json(['deleted' => $id, 'result' => $store]); 
    }
}
