<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function storage(){
        return $this->hasMany('App\Storage', 'id', 'user_id');
    }

    public function log(){
        return $this->hasMany('App\Logs', 'id', 'log_id');
    }

    public function comments(){
        return $this->hasMany('App\Comments', 'id', 'comment_id');
    }
}
