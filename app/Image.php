<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class Image extends Model
{
	protected $table = 'storage';

    public function user(){
    	return $this->hasOne('App\User', 'user_id', 'id');
    }

    public function data(){
        return json_decode($this->data);
    }
}
